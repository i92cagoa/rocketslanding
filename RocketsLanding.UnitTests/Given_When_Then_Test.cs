using System;

namespace RocketsLanding.UnitTests
{
    public abstract class Given_When_Then_Test
        : IDisposable
    {
        protected Given_When_Then_Test()
        {
            this.Setup();
        }

        private void Setup()
        {
            this.Given();
            this.When();
        }

        protected abstract void Given();

        protected abstract void When();

        public void Dispose()
        {
            this.Cleanup();
        }

        protected virtual void Cleanup()
        {
        }
    }
}