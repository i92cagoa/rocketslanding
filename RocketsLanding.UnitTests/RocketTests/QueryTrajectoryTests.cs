using FluentAssertions;
using Moq;
using RocketsLanding.Lib;
using RocketsLanding.Lib.Contracts;
using Xunit;

namespace RocketsLanding.UnitTests.RocketTests
{
    public static class QueryTrajectoryTests
    {
        public class Given_Rocket_Landing_Position_5_5_When_Querying_Trajectory
            : Given_When_Then_Test
        {
            private Rocket _sut;
            private Mock<ILandingPlatform> _landingPlatformMock;
            private RocketLandingPosition _rocketLandingPosition;
            private string _result;

            protected override void Given()
            {
                _landingPlatformMock = new Mock<ILandingPlatform>();
                _landingPlatformMock
                    .Setup(x => x.CheckLandingPosition(It.IsAny<RocketLandingPosition>()))
                    .Returns("Ok for landing");

                var landingPlatform = _landingPlatformMock.Object;
                _sut = new Rocket(landingPlatform);
                _rocketLandingPosition = new RocketLandingPosition(5, 5);
            }

            protected override void When()
            {
                _result = _sut.QueryTrajectory(_rocketLandingPosition);
            }

            [Fact]
            public void Then_It_Should_Return_Ok_For_Landing()
            {
                _result.Should().Be("Ok for landing");
            }

            [Fact]
            public void Then_It_Should_Use_Landing_Platform_CheckLandingPosition()
            {
                _landingPlatformMock.Verify(x => x.CheckLandingPosition(It.IsAny<RocketLandingPosition>()), Times.Once);
            }
        }
    }
}