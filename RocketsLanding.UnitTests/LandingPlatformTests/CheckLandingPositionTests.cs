using FluentAssertions;
using RocketsLanding.Lib;
using Xunit;

namespace RocketsLanding.UnitTests.LandingPlatformTests
{
    public static class CheckLandingPositionTests
    {
        public class Given_Platform_Size_10_10_And_Platform_Starting_Position_5_5_And_No_Previous_Rocket_Checked_And_Rocket_Landing_Position_5_5_When_Checking_Trajectory
            : Given_When_Then_Test
        {
            private LandingPlatform _sut;
            private RocketLandingPosition _landingPosition;
            private string _result;

            protected override void Given()
            {
                var platformSize = new PlatformSize(10, 10);
                var platformStartingPosition = new PlatformStartingPosition(5, 5);
                _sut = new LandingPlatform(platformSize, platformStartingPosition);
                _landingPosition = new RocketLandingPosition(5, 5);
            }

            protected override void When()
            {
                _result = _sut.CheckLandingPosition(_landingPosition);
            }

            [Fact]
            public void Then_It_Should_Return_Ok_For_Landing()
            {
                _result.Should().Be("Ok for landing");
            }
        }

        public class Given_Platform_Size_10_10_And_Platform_Starting_Position_5_5_And_No_Previous_Rocket_Checked_And_Rocket_Landing_Position_8_9_When_Checking_Trajectory
            : Given_When_Then_Test
        {
            private LandingPlatform _sut;
            private RocketLandingPosition _landingPosition;
            private string _result;

            protected override void Given()
            {
                var platformSize = new PlatformSize(10, 10);
                var platformStartingPosition = new PlatformStartingPosition(5, 5);
                _sut = new LandingPlatform(platformSize, platformStartingPosition);
                _landingPosition = new RocketLandingPosition(8, 9);
            }

            protected override void When()
            {
                _result = _sut.CheckLandingPosition(_landingPosition);
            }

            [Fact]
            public void Then_It_Should_Return_Ok_For_Landing()
            {
                _result.Should().Be("Ok for landing");
            }
        }

        public class Given_Platform_Size_10_10_And_Platform_Starting_Position_5_5_And_No_Previous_Rocket_Checked_Rocket_Landing_Position_16_15_When_Checking_Trajectory
            : Given_When_Then_Test
        {
            private LandingPlatform _sut;
            private RocketLandingPosition _landingPosition;
            private string _result;

            protected override void Given()
            {
                var platformSize = new PlatformSize(10, 10);
                var platformStartingPosition = new PlatformStartingPosition(5, 5);
                _sut = new LandingPlatform(platformSize, platformStartingPosition);
                _landingPosition = new RocketLandingPosition(16, 15);
            }

            protected override void When()
            {
                _result = _sut.CheckLandingPosition(_landingPosition);
            }

            [Fact]
            public void Then_It_Should_Return_Out_Of_Platform()
            {
                _result.Should().Be("Out of platform");
            }
        }

        public class Given_Platform_Size_10_10_And_Platform_Starting_Position_5_5_And_No_Previous_Rocket_Checked_Rocket_Landing_Position_4_5_When_Checking_Trajectory
            : Given_When_Then_Test
        {
            private LandingPlatform _sut;
            private RocketLandingPosition _landingPosition;
            private string _result;

            protected override void Given()
            {
                var platformSize = new PlatformSize(10, 10);
                var platformStartingPosition = new PlatformStartingPosition(5, 5);
                _sut = new LandingPlatform(platformSize, platformStartingPosition);
                _landingPosition = new RocketLandingPosition(4, 5);
            }

            protected override void When()
            {
                _result = _sut.CheckLandingPosition(_landingPosition);
            }

            [Fact]
            public void Then_It_Should_Return_Out_Of_Platform()
            {
                _result.Should().Be("Out of platform");
            }
        }

        public class Given_Platform_Size_10_10_And_Platform_Starting_Position_5_5_And_No_Previous_Rocket_Checked_Rocket_Landing_Position_5_4_When_Checking_Trajectory
            : Given_When_Then_Test
        {
            private LandingPlatform _sut;
            private RocketLandingPosition _landingPosition;
            private string _result;

            protected override void Given()
            {
                var platformSize = new PlatformSize(10, 10);
                var platformStartingPosition = new PlatformStartingPosition(5, 5);
                _sut = new LandingPlatform(platformSize, platformStartingPosition);
                _landingPosition = new RocketLandingPosition(5, 4);
            }

            protected override void When()
            {
                _result = _sut.CheckLandingPosition(_landingPosition);
            }

            [Fact]
            public void Then_It_Should_Return_Out_Of_Platform()
            {
                _result.Should().Be("Out of platform");
            }
        }

        public class Given_Platform_Size_10_10_And_Platform_Starting_Position_5_5_And_No_Previous_Rocket_Checked_Rocket_Landing_Position_15_16_When_Checking_Trajectory
            : Given_When_Then_Test
        {
            private LandingPlatform _sut;
            private RocketLandingPosition _landingPosition;
            private string _result;

            protected override void Given()
            {
                var platformSize = new PlatformSize(10, 10);
                var platformStartingPosition = new PlatformStartingPosition(5, 5);
                _sut = new LandingPlatform(platformSize, platformStartingPosition);
                _landingPosition = new RocketLandingPosition(15, 16);
            }

            protected override void When()
            {
                _result = _sut.CheckLandingPosition(_landingPosition);
            }

            [Fact]
            public void Then_It_Should_Return_Out_Of_Platform()
            {
                _result.Should().Be("Out of platform");
            }
        }

        public class Given_Platform_Size_3_3_And_Platform_Starting_Position_10_10_And_No_Previous_Rocket_Checked_Rocket_Landing_Position_11_12_When_Checking_Trajectory
            : Given_When_Then_Test
        {
            private LandingPlatform _sut;
            private RocketLandingPosition _landingPosition;
            private string _result;

            protected override void Given()
            {
                var platformSize = new PlatformSize(3, 3);
                var platformStartingPosition = new PlatformStartingPosition(10, 10);
                _sut = new LandingPlatform(platformSize, platformStartingPosition);
                _landingPosition = new RocketLandingPosition(11, 12);
            }

            protected override void When()
            {
                _result = _sut.CheckLandingPosition(_landingPosition);
            }

            [Fact]
            public void Then_It_Should_Return_Ok_For_Landing()
            {
                _result.Should().Be("Ok for landing");
            }
        }

        public class Given_Platform_Size_5_5_And_Platform_Starting_Position_3_3_And_Previous_Rocket_With_Position_5_5_And_New_Rocket_Landing_Position_6_6_When_Checking_Trajectory
            : Given_When_Then_Test
        {
            private LandingPlatform _sut;
            private RocketLandingPosition _landingPosition;
            private string _result;

            protected override void Given()
            {
                var platformSize = new PlatformSize(5, 5);
                var platformStartingPosition = new PlatformStartingPosition(3, 3);
                _sut = new LandingPlatform(platformSize, platformStartingPosition);
                var previousLandingPosition = new RocketLandingPosition(5, 5);
                _sut.CheckLandingPosition(previousLandingPosition);

                _landingPosition = new RocketLandingPosition(6, 6);
            }

            protected override void When()
            {
                _result = _sut.CheckLandingPosition(_landingPosition);
            }

            [Fact]
            public void Then_It_Should_Return_Clash()
            {
                _result.Should().Be("Clash");
            }
        }

        public class Given_Platform_Size_5_5_And_Platform_Starting_Position_3_3_And_Previous_Rocket_With_Position_5_5_And_New_Rocket_Landing_Position_5_6_When_Checking_Trajectory
            : Given_When_Then_Test
        {
            private LandingPlatform _sut;
            private RocketLandingPosition _landingPosition;
            private string _result;

            protected override void Given()
            {
                var platformSize = new PlatformSize(5, 5);
                var platformStartingPosition = new PlatformStartingPosition(3, 3);
                _sut = new LandingPlatform(platformSize, platformStartingPosition);
                var previousLandingPosition = new RocketLandingPosition(5, 5);
                _sut.CheckLandingPosition(previousLandingPosition);

                _landingPosition = new RocketLandingPosition(5, 6);
            }

            protected override void When()
            {
                _result = _sut.CheckLandingPosition(_landingPosition);
            }

            [Fact]
            public void Then_It_Should_Return_Clash()
            {
                _result.Should().Be("Clash");
            }
        }

        public class Given_Platform_Size_5_5_And_Platform_Starting_Position_3_3_And_Previous_Rocket_With_Position_5_5_And_New_Rocket_Landing_Position_6_5_When_Checking_Trajectory
            : Given_When_Then_Test
        {
            private LandingPlatform _sut;
            private RocketLandingPosition _landingPosition;
            private string _result;

            protected override void Given()
            {
                var platformSize = new PlatformSize(5, 5);
                var platformStartingPosition = new PlatformStartingPosition(3, 3);
                _sut = new LandingPlatform(platformSize, platformStartingPosition);
                var previousLandingPosition = new RocketLandingPosition(5, 5);
                _sut.CheckLandingPosition(previousLandingPosition);

                _landingPosition = new RocketLandingPosition(6, 5);
            }

            protected override void When()
            {
                _result = _sut.CheckLandingPosition(_landingPosition);
            }

            [Fact]
            public void Then_It_Should_Return_Clash()
            {
                _result.Should().Be("Clash");
            }
        }

        public class Given_Platform_Size_5_5_And_Platform_Starting_Position_3_3_And_Previous_Rocket_With_Position_5_5_And_New_Rocket_Landing_Position_5_5_When_Checking_Trajectory
            : Given_When_Then_Test
        {
            private LandingPlatform _sut;
            private RocketLandingPosition _landingPosition;
            private string _result;

            protected override void Given()
            {
                var platformSize = new PlatformSize(5, 5);
                var platformStartingPosition = new PlatformStartingPosition(3, 3);
                _sut = new LandingPlatform(platformSize, platformStartingPosition);
                var previousLandingPosition = new RocketLandingPosition(5, 5);
                _sut.CheckLandingPosition(previousLandingPosition);

                _landingPosition = new RocketLandingPosition(5, 5);
            }

            protected override void When()
            {
                _result = _sut.CheckLandingPosition(_landingPosition);
            }

            [Fact]
            public void Then_It_Should_Return_Clash()
            {
                _result.Should().Be("Clash");
            }
        }

        public class Given_Platform_Size_5_5_And_Platform_Starting_Position_3_3_And_Previous_Rocket_With_Position_5_5_And_New_Rocket_Landing_Position_4_4_When_Checking_Trajectory
            : Given_When_Then_Test
        {
            private LandingPlatform _sut;
            private RocketLandingPosition _landingPosition;
            private string _result;

            protected override void Given()
            {
                var platformSize = new PlatformSize(5, 5);
                var platformStartingPosition = new PlatformStartingPosition(3, 3);
                _sut = new LandingPlatform(platformSize, platformStartingPosition);
                var previousLandingPosition = new RocketLandingPosition(5, 5);
                _sut.CheckLandingPosition(previousLandingPosition);

                _landingPosition = new RocketLandingPosition(4, 4);
            }

            protected override void When()
            {
                _result = _sut.CheckLandingPosition(_landingPosition);
            }

            [Fact]
            public void Then_It_Should_Return_Clash()
            {
                _result.Should().Be("Clash");
            }
        }

        public class Given_Platform_Size_5_5_And_Platform_Starting_Position_3_3_And_Previous_Rocket_With_Position_5_5_And_New_Rocket_Landing_Position_4_5_When_Checking_Trajectory
            : Given_When_Then_Test
        {
            private LandingPlatform _sut;
            private RocketLandingPosition _landingPosition;
            private string _result;

            protected override void Given()
            {
                var platformSize = new PlatformSize(5, 5);
                var platformStartingPosition = new PlatformStartingPosition(3, 3);
                _sut = new LandingPlatform(platformSize, platformStartingPosition);
                var previousLandingPosition = new RocketLandingPosition(5, 5);
                _sut.CheckLandingPosition(previousLandingPosition);

                _landingPosition = new RocketLandingPosition(4, 5);
            }

            protected override void When()
            {
                _result = _sut.CheckLandingPosition(_landingPosition);
            }

            [Fact]
            public void Then_It_Should_Return_Clash()
            {
                _result.Should().Be("Clash");
            }
        }

        public class Given_Platform_Size_5_5_And_Platform_Starting_Position_3_3_And_Previous_Rocket_With_Position_5_5_And_New_Rocket_Landing_Position_5_4_When_Checking_Trajectory
            : Given_When_Then_Test
        {
            private LandingPlatform _sut;
            private RocketLandingPosition _landingPosition;
            private string _result;

            protected override void Given()
            {
                var platformSize = new PlatformSize(5, 5);
                var platformStartingPosition = new PlatformStartingPosition(3, 3);
                _sut = new LandingPlatform(platformSize, platformStartingPosition);
                var previousLandingPosition = new RocketLandingPosition(5, 5);
                _sut.CheckLandingPosition(previousLandingPosition);

                _landingPosition = new RocketLandingPosition(5, 4);
            }

            protected override void When()
            {
                _result = _sut.CheckLandingPosition(_landingPosition);
            }

            [Fact]
            public void Then_It_Should_Return_Clash()
            {
                _result.Should().Be("Clash");
            }
        }

        public class Given_Platform_Size_4_4_And_Platform_Starting_Position_5_5_And_Previous_Rocket_With_Position_5_5_And_New_Rocket_Landing_Position_8_7_When_Checking_Trajectory
            : Given_When_Then_Test
        {
            private LandingPlatform _sut;
            private RocketLandingPosition _landingPosition;
            private string _result;

            protected override void Given()
            {
                var platformSize = new PlatformSize(4, 4);
                var platformStartingPosition = new PlatformStartingPosition(5, 5);
                _sut = new LandingPlatform(platformSize, platformStartingPosition);
                var previousLandingPosition = new RocketLandingPosition(5, 5);
                _sut.CheckLandingPosition(previousLandingPosition);

                _landingPosition = new RocketLandingPosition(8, 7);
            }

            protected override void When()
            {
                _result = _sut.CheckLandingPosition(_landingPosition);
            }

            [Fact]
            public void Then_It_Should_Return_Ok_For_Landing()
            {
                _result.Should().Be("Ok for landing");
            }
        }
    }
}