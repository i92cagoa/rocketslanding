using RocketsLanding.Lib.Contracts;

namespace RocketsLanding.Lib
{
    public class Rocket
    {
        private readonly ILandingPlatform _landingPlatform;

        public Rocket(ILandingPlatform landingPlatform)
        {
            _landingPlatform = landingPlatform;
        }

        public string QueryTrajectory(RocketLandingPosition rocketLandingPosition)
        {
            var queryTrajectoryResult = _landingPlatform.CheckLandingPosition(rocketLandingPosition);

            return queryTrajectoryResult;
        }
    }
}