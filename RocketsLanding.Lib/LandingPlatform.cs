using RocketsLanding.Lib.Contracts;

namespace RocketsLanding.Lib
{
    public class LandingPlatform
        : ILandingPlatform
    {
        private PlatformSize _platformSize;
        private PlatformStartingPosition _platformStartingPosition;
        private RocketLandingPosition _previousRocketLandingPosition;

        public LandingPlatform(PlatformSize platformSize, PlatformStartingPosition startingPosition)
        {
            _platformSize = platformSize;
            _platformStartingPosition = startingPosition;
        }

        public string CheckLandingPosition(RocketLandingPosition landingPosition)
        {
            if(LandingPositionClashesWithPreviousRocket(landingPosition))
            {
                _previousRocketLandingPosition = landingPosition;
                
                return "Clash";
            }

            var landingPositionResult = "Out of platform";
            if(LandingPositionIsInsidePlatform(landingPosition))
            {
                landingPositionResult = "Ok for landing";
            }

            _previousRocketLandingPosition = landingPosition;

            return landingPositionResult;
        }

        private bool LandingPositionIsInsidePlatform(RocketLandingPosition landingPosition)
        {
            var landingPositionInsidePlatform = 
                landingPosition.X >= _platformStartingPosition.X 
                && landingPosition.Y >= _platformStartingPosition.Y
                && landingPosition.X <= _platformStartingPosition.X + _platformSize.X
                && landingPosition.Y <= _platformStartingPosition.Y + _platformSize.Y;

            if(landingPositionInsidePlatform)
            {
                return true;
            }

            return false;
        }

        private bool LandingPositionClashesWithPreviousRocket(RocketLandingPosition landingPosition)
        {
            var landingPositionClashes = 
                landingPosition.X - 1 == _previousRocketLandingPosition?.X
                || landingPosition.Y - 1 == _previousRocketLandingPosition?.Y
                || landingPosition.X == _previousRocketLandingPosition?.X
                || landingPosition.Y == _previousRocketLandingPosition?.Y
                || landingPosition.X + 1 == _previousRocketLandingPosition?.X
                || landingPosition.Y + 1 == _previousRocketLandingPosition?.Y;

            if(landingPositionClashes)
            {
                return true;
            }

            return false;
        }
    }
}