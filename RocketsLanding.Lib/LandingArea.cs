using System.Collections.Generic;

namespace RocketsLanding.Lib
{
    public class LandingArea
    {
        private readonly List<LandingPlatform> _landingPlatforms;
        private readonly List<Rocket> _rockets;
        private AreaSize _landingAreaSize;

        public LandingArea()
        {
            var platformSize = new PlatformSize(5, 5);
            var startingPosition = new PlatformStartingPosition(3, 3);
            var landingPlatform = new LandingPlatform(platformSize, startingPosition);
            _landingAreaSize = new AreaSize(100, 100);
            _landingPlatforms = 
                new List<LandingPlatform>
                {
                    landingPlatform
                };

            _rockets = 
                new List<Rocket>
                {
                    new Rocket(landingPlatform)
                };
        }
    }
}