using System;

namespace RocketsLanding.Lib
{
    public class PlatformStartingPosition
    {
        public int X { get;}
        public int Y { get;}

        public PlatformStartingPosition(int x, int y)
        {
            if(x < 0 || y < 0)
            {
                throw new ArgumentException("Wrong values for Platform starting position");
            }
            
            X = x;
            Y = y;
        }
    }
}