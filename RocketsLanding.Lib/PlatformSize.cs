namespace RocketsLanding.Lib
{
    public class PlatformSize
        : AreaSize
    {
        public PlatformSize(int x, int y) 
            : base(x, y)
        {
        }
    }
}