using System;

namespace RocketsLanding.Lib
{
    public class AreaSize
    {
        public int X { get;}
        public int Y { get;}

        public AreaSize(int x, int y)
        {
            if(x < 1 || y < 1)
            {
                throw new ArgumentException("Wrong values for Area size");
            }

            X = x;
            Y = y;
        }
    }
}