namespace RocketsLanding.Lib.Contracts
{
    public interface ILandingPlatform
    {
        string CheckLandingPosition(RocketLandingPosition landingPosition);
    }
}