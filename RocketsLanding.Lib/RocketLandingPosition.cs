using System;

namespace RocketsLanding.Lib
{
    public class RocketLandingPosition
    {
        public int X { get; }
        public int Y { get; }

        public RocketLandingPosition(int x, int y)
        {
            if(x < 0 || y < 0)
            {
                throw new ArgumentException("Wrong values for Rocket landing position");
            }
            
            X = x;
            Y = y;
        }
    }
}